import {
  BadRequestException,
  ConflictException,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Logger,
  Post,
  Req,
  Request,
  Res,
  UseGuards,
} from '@nestjs/common';
import { UserService } from './user.service';
import { sign } from 'jsonwebtoken';
import { ThrottlerGuard } from '@nestjs/throttler';
import { userRemoveSchema, userSchema } from './validator/user.validator';
import { JwtAuthGuard } from 'src/guard/jwt-auth.guard';
import { RoleGuard } from 'src/guard/role.guard';
import { Roles } from 'src/guard/roles.decorator';

@Controller('user')
export class UserController {
  logger: Logger;
  constructor(private readonly userService: UserService) {
    this.logger = new Logger(UserController.name);
  }

  @Post('create')
  @UseGuards(ThrottlerGuard)
  async create(@Request() req, @Res() res): Promise<any> {
    const newUser = req.body;
    const result = userSchema.validate(newUser, {
      convert: true,
    });

    if (result.error) {
      const errorMessages = result.error.details.map((d) => d.message).join();
      throw new BadRequestException(errorMessages);
    }
    try {
      const query = { email: newUser.email };
      const isUser = await this.userService.findOne(query);
      if (isUser) throw new ConflictException('User Already Exist');
      const user = await this.userService.create(newUser);
      const token = await sign(
        {
          _id: user?._id,
          email: user?.email,
          role: user?.role,
        },
        process.env.JWT_SECRETE,
      );
      return res.status(HttpStatus.OK).json({ token, role: user?.role });
    } catch (err) {
      this.logger.error('Something went wrong in signup:', err);
      throw err;
    }
  }

  @Roles('admin')
  @UseGuards(JwtAuthGuard, RoleGuard, ThrottlerGuard)
  @Get()
  async profile(@Req() req, @Res() res) {
    const data = req.query;
    const users = await this.userService.find(data, {
      perPage: data?.perPage,
      page: data?.page,
      sort: data?.sort,
    });
    return res.status(HttpStatus.OK).json(users);
  }

  @Roles('admin')
  @UseGuards(JwtAuthGuard, RoleGuard, ThrottlerGuard)
  @Delete()
  async profileDelete(@Req() req, @Res() res) {
    const data = req.body;
    const result = userRemoveSchema.validate(data, {
      convert: true,
    });

    if (result.error) {
      const errorMessages = result.error.details.map((d) => d.message).join();
      throw new BadRequestException(errorMessages);
    }
    const isUser = await this.userService.findOne(data);
    if (!isUser) throw new BadRequestException('User Not found!');

    const response = await this.userService.findOneAndRemove(data);
    return res.status(HttpStatus.OK).json(response);
  }
}

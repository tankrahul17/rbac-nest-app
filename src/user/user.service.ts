import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { User, UserDocument } from './model/user.model';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import * as bcrypt from 'bcrypt';

@Injectable()
export class UserService {
  logger: Logger;
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {
    this.logger = new Logger(UserService.name);
  }

  async findOne(query: any): Promise<any> {
    return await this.userModel.findOne(query).select('+password');
  }

  async find(
    usersFilterQuery: FilterQuery<User>,
    pagination: any,
  ): Promise<User[]> {
    delete usersFilterQuery?.perPage;
    delete usersFilterQuery?.page;
    delete usersFilterQuery?.sort;
    let sort: any = { email: 1 };
    if (pagination.sort) {
      sort = pagination.sort;
    }
    return await this.userModel
      .find()
      .sort(sort)
      .skip(pagination?.perPage * pagination?.page)
      .limit(pagination?.perPage);
  }

  async getHashedPassword(password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, (err, hash) => {
        if (err) {
          reject(err);
        }
        resolve(hash);
      });
    });
  }

  async create(user: any): Promise<any> {
    this.logger.log('Creating user.');
    if (user.facebookId || user.googleId) return this.userModel.create(user);

    const hashedPassword = await this.getHashedPassword(user.password);
    user.password = hashedPassword;
    const newUser = new this.userModel(user);
    return newUser.save();
  }

  async findOneAndUpdate(query: any, payload: any): Promise<User> {
    this.logger.log('Updating User.');
    return this.userModel.findOneAndUpdate(query, payload, {
      new: true,
      upsert: true,
    });
  }

  async findOneAndRemove(query: any): Promise<any> {
    return this.userModel.findOneAndDelete(query);
  }
}

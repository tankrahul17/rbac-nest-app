import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema({ autoSearchIndex: true })
export class User {
  @Prop()
  role: string;

  @Prop({ lowercase: true, unique: true, index: true })
  email: string;

  @Prop({ select: false })
  password: string;
}

export type UserDocument = User & Document;

export const UserSchema = SchemaFactory.createForClass(User);

import Joi from 'joi';

export const userSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  role: Joi.string().valid('user', 'admin').required(),
  password: Joi.string().min(5).alphanum().required(),
});

export const userRemoveSchema = Joi.object().keys({
  email: Joi.string().email().required(),
});

export const loginSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  password: Joi.string().min(5).alphanum().required(),
});

import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
  forwardRef,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { sign } from 'jsonwebtoken';
import { AuthenticateDto } from './dto/authenticate.dto';
import { IAuthenticate } from './interfaces/user.interface';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { UserService } from 'src/user/user.service';

@Injectable()
export class AuthService {
  constructor(
    @Inject(forwardRef(() => UserService))
    private UserService: UserService,
  ) {}

  async authenticate(authenticateDto: AuthenticateDto): Promise<IAuthenticate> {
    const user = await this.validateUser(
      authenticateDto.email,
      authenticateDto.password,
    );
    if (!user) {
      throw new BadRequestException('Invalid credentials');
    }

    const token = await sign(
      {
        _id: user?._id,
        email: user?.email,
        role: user?.role,
      },
      process.env.JWT_SECRETE,
    );
    return { token, role: user?.role };
  }

  async validateUser(email: string, pass: string): Promise<any> {
    const query = { email: email };
    const user = await this.UserService.findOne(query);
    if (!user) {
      throw new NotFoundException('Email Does not exist');
    }
    const isMatched = await this.comparePasswords(pass, user.password);
    if (!isMatched) {
      throw new UnauthorizedException('Invalid Password');
    }
    return user;
  }

  async generateJwtToken(user: any): Promise<any> {
    return sign({ ...user }, process.env.JWT_SECRETE);
  }

  async getHashedPassword(password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      bcrypt.hash(password, 10, (err, hash) => {
        if (err) {
          reject(err);
        }
        resolve(hash);
      });
    });
  }

  async comparePasswords(
    password: string,
    hashedPassword: string,
  ): Promise<any> {
    return bcrypt
      .compare(password, hashedPassword)
      .then((isMatch) => {
        if (isMatch) return true;
        return false;
      })
      .catch((err) => err);
  }
}

import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpStatus,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthenticateDto } from './dto/authenticate.dto';
import { JwtAuthGuard } from '../guard/jwt-auth.guard';
import { RoleGuard } from '../guard/role.guard';
import { Roles } from '../guard/roles.decorator';
import { ThrottlerGuard } from '@nestjs/throttler';
import { loginSchema } from 'src/user/validator/user.validator';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post()
  @UseGuards(ThrottlerGuard)
  async login(@Res() res, @Body() authenticateDto: AuthenticateDto) {
    try {
      const result = loginSchema.validate(authenticateDto, {
        convert: true,
      });
      if (result.error) {
        const errorMessages = result.error.details.map((d) => d.message).join();
        throw new BadRequestException(errorMessages);
      }
      const response = await this.authService.authenticate(authenticateDto);
      return res.status(HttpStatus.OK).json(response);
    } catch (error) {
      return res.status(error.status).json(error.response);
    }
  }

  @Roles('user')
  @UseGuards(JwtAuthGuard, RoleGuard, ThrottlerGuard)
  @Get()
  profile(@Req() req, @Res() res) {
    return res.status(HttpStatus.OK).json(req.user);
  }
}

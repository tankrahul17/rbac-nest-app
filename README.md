Overview
This repository contains two projects: an Angular application and a NestJS application. Follow the instructions below to set up and run each project.

NestJS Application
Directory: nest-app

Navigate to the NestJS project directory: 
`cd nest-app`

Install the necessary packages:
`npm install`

Start the NestJS application:
`npm run start`

The application will be available at http://localhost:3000.

Notes
Ensure you have Node.js and npm installed on your system before running the above commands.
If you encounter any issues, check the respective project documentation or raise an issue in the repository.